# Zeb’s freeCodeCamp All about Bob Tribute Page

## Description

A tribute page to Bob from LEGO Universe created for a freeCodeCamp project.

## Licensing info

Image of Bob sourced from [LEGO Universe Wiki](https://legouniverse.fandom.com/wiki/Bob).

Everything else is © 2018-2019 Zebulan Stanphill, released under the GNU General Public License version 3 (or any later version). See `gpl-3.0.txt` or view the license online: https://www.gnu.org/licenses/gpl-3.0.html

The textual content of this work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

This project uses the Nunito font:

https://github.com/vernnobile/NunitoFont

© 2014 Vernon Adams (vern@newtypography.co.uk).

Released under the SIL Open Font License, Version 1.1. See `nunito-license.txt`.